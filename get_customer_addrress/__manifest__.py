# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Display vendor/customer name  with address on form',
    'version': '2.0',
    'category': 'Marketing',
    'complexity': 'easy',
    'website': '',
    'description': """This module shows  partners/vendor/customer name along with their address on purchase,vendor,pipeline and on transfer form.This module Works on both community and enterprise version 11""",
    'depends': ['purchase','crm'],
    'data': [
        'views/purchase_view.xml',
    ],
    'installable': True,
    'auto_install': True,
    'images': ['static/description/background.png',],   
    'license': 'LGPL-3',
    "price":9,
    "currency": "EUR"    
}
